function execShellCommand(cmd) {
  const exec = require("child_process").exec;
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.warn(error);
      }
      resolve(stdout ? stdout : stderr);
    });
  });
}

const helper = async () => {
  const result = await execShellCommand("git pull");
  if (result.trim() == "Already up to date.") return true;
  process.exit(1);
  return false;
};

module.exports = helper;
