const request = require('request-promise');

const GITLAB_TOKEN=process.env.GITLAB_TOKEN;
const PROJECT_ID=13607488; // TODO enviornment variable?

class Gitlab{
  constructor(token, pid){
    this.token = token;
    this.pid = pid;
  }

  async mergeRequests(){
    return request.get(`https://gitlab.com/api/v4/projects/${this.pid}/merge_requests`, {
      headers:{
        'Private-Token': this.token,
      },
    });
  }
}

module.exports = new Gitlab(GITLAB_TOKEN, PROJECT_ID);
