const RestartHelper = require("./RestartHelper");

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function run() {
  await RestartHelper();
  console.log(`I'm still awake`);
  await sleep(2000);
}

run();
